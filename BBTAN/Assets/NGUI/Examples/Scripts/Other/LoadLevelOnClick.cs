using UnityEngine;
using System.Collections.Generic;

[AddComponentMenu("NGUI/Examples/Load Level On Click")]
public class LoadLevelOnClick : MonoBehaviour
{
	public string levelName;

    void Awake()
    {
    }

	public void OnClick ()
	{
		if (!string.IsNullOrEmpty(levelName))
            Application.LoadLevelAsync(levelName);
	}
}