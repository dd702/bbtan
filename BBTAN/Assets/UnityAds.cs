﻿using System;
using UnityEngine;
using UnityEngine.Advertisements;

public class UnityAds : MonoBehaviour {

    private bool IsTest = true;
    private Action ActionAdsFinished = null;

    public void SetActionAdsFinished (Action actionAdsFinished)
    {
        Advertisement.Initialize("1101714", true);
        ActionAdsFinished = actionAdsFinished;
    }

    public void ShowRewardedAd()
    {
#if UNITY_EDITOR || UNITY_STANDALONE
        ActionAdsFinished();
#elif UNITY_ANDROID || UNITY_IOS
        if (Advertisement.IsReady("rewardedVideo"))
        {
            var options = new ShowOptions { resultCallback = HandleShowResult };
            Advertisement.Show("rewardedVideo", options);
        }
#endif
    }

    private void HandleShowResult(ShowResult result)
    {
        switch (result)
        {
#if UNITY_EDITOR || UNITY_STANDALONE
            case ShowResult.Finished:
                ActionAdsFinished();
                Debug.Log("The ad was successfully shown.");
                //
                // YOUR CODE TO REWARD THE GAMER
                // Give coins etc.
                break;
            case ShowResult.Skipped:
                Debug.Log("The ad was skipped before reaching the end.");
                break;
            case ShowResult.Failed:
                Debug.LogError("The ad failed to be shown.");
                break;
#elif UNITY_ANDROID || UNITY_IOS
            case ShowResult.Finished:
                ActionAdsFinished();
                break;
            case ShowResult.Skipped:
                break;
            case ShowResult.Failed:
                break;
#endif
        }
    }
}