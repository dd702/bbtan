﻿using UnityEngine;
using System.Collections;

public class UIAnimation : MonoBehaviour {

	public void PlaySoundStartGame () { GamePlayManager.GetInstance.RefSoundManager.PlaySoundOneShot(SoundManager.StrGameStart); }
    public void StartGame () { GamePlayManager.GetInstance.GameStart(); }
    public void StartFirstTurn () { GamePlayManager.GetInstance.CallNextTurn(); }
}
