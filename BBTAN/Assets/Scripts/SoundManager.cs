﻿using UnityEngine;
using System.Collections.Generic;

public class SoundManager : MonoBehaviour {

    public const string StrDictionaryPop    = "Pop";
    public const string StrLaser            = "laser";
    public const string StrAddBullet        = "AddBullet";
    public const string StrCoin             = "Coin";
    public const string StrChangeDirection  = "ChangeDirection";
    public const string StrGameEnd          = "GameEnd";
    public const string StrGameStart        = "GameStart";
    public const string StrButtonClick      = "ButtonClick";

    //public const string StrDictionary

    public AudioSource SoundPlayer = null;
    private Dictionary<string, AudioClip> DicSounds = new Dictionary<string, AudioClip>();

    public List<AudioClip> listAudioClips = null;

	void Awake ()
    {
        Init();
    }

    void Init ()
    {
        SoundPlayer = GetComponent<AudioSource>();

        for (int i = 0; i < listAudioClips.Count; ++i)
            DicSounds.Add(listAudioClips[i].name, listAudioClips[i]);
    }

    public void PlaySoundOneShot(string strFileName)
    {
        SoundPlayer.PlayOneShot(DicSounds[strFileName]);
    }

    public void SwitchAudioMute ()
    {
        SoundPlayer.mute = !SoundPlayer.mute;
    }
}
