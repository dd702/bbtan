﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour
{
    private const string StrBottomWall = "BottomWall";

    private Vector3 VecDirMoving = Vector3.zero;
    private float Speed = 1.5f;
    private bool IsBulletLandBottomFirst = false;
    private BulletManager BulletManager = null;
    private CircleCollider2D BulletCollider = null;
    private Rigidbody2D BulletRigidBody = null;
    private Vector3 PosFirstLandedBullet;

    public bool PropIsBulletLandBottomFirst { get { return IsBulletLandBottomFirst; } set { IsBulletLandBottomFirst = value; } }

    void Awake()
    {
        BulletManager = GameObject.FindGameObjectWithTag("BulletManager").GetComponent<BulletManager>();
        BulletCollider = GetComponent<CircleCollider2D>();
        BulletRigidBody = GetComponent<Rigidbody2D>();

        Speed = GamePlayManager.BallSpd;
    }

    public void Move(Vector3 vecDirMoving, float speed)
    {
        VecDirMoving = vecDirMoving.normalized;

        BulletRigidBody.velocity = new Vector2(VecDirMoving.x, VecDirMoving.y) * Speed;
    }

    IEnumerator CalcMoving()
    {
        while (true)
        {
            // Move In Normal State
            transform.localPosition += VecDirMoving * Speed;
            yield return null;
        }
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.name == StrBottomWall)
        {
            // All Bullets -> Stop
            BulletRigidBody.velocity = new Vector2(0.0f, 0.0f);
            BulletManager refBulletManager = GamePlayManager.GetInstance.RefBulletManager;
            // Land 1st Bullet -> Notify to AllBullets & Move Monster to Ball
            if (!IsBulletLandBottomFirst)
                BulletManager.NotifyLandingFirstBullet(transform);

            // Land 2nd ~ Bullets -> Move Ball to Player
            else
                MoveBallsToPlayer();

            //Check Condition LastBulletLanded
            if (GamePlayManager.GetInstance.FireCount == refBulletManager.CountLandedBullet)
                GamePlayManager.GetInstance.CallNextTurn();
            else
                ++refBulletManager.CountLandedBullet;
        }
    }

    public void LandFirstBullet(Vector3 firstLandedBullet)
    {
        IsBulletLandBottomFirst = true;
        PosFirstLandedBullet = firstLandedBullet;
    }

    void MoveBallsToPlayer()
    {
        Vector3 pos = transform.localPosition;
        TweenPosition moveToPlayer = GetComponent<TweenPosition>();
        moveToPlayer.ResetToBeginning();
        moveToPlayer.from = pos;
        moveToPlayer.to = PosFirstLandedBullet;
        moveToPlayer.PlayForward();
    }
}