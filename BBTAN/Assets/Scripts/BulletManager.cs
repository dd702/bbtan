﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BulletManager : MonoBehaviour
{

    private List<Bullet> BulletList = new List<Bullet>();

    public TweenPosition PlayerTweenPos = null;
    public GameObject PrefabBullet = null;

    private bool IsBulletLandBottomFirst = true;

    private int nCountLandedBullet;
    public int CountLandedBullet
    {
        get { return nCountLandedBullet; }
        set
        {
            nCountLandedBullet = value;
            GamePlayManager.GetInstance.PropLabelBallCount = value.ToString();
        }
    }

    void Awake()
    {
        AddBullets();
    }

    public void Fire(Vector3 towards, int fireCount, float fireInterval)
    {
        while (fireCount > BulletList.Count)
            AddBullets();

        for (int i = 0; i < fireCount; ++i)
        {
            BulletList[i].gameObject.SetActive(true);
            BulletList[i].transform.parent = PlayerTweenPos.transform;
            BulletList[i].transform.localPosition = new Vector3(-120.0f, 20.0f, 0.0f);
            BulletList[i].transform.parent = transform;
            BulletList[i].PropIsBulletLandBottomFirst = false;
        }

        GamePlayManager.GetInstance.GameState = eGameState.eCantShoot;
        StartCoroutine(FireBulletByInterval(towards, fireCount, fireInterval));
    }

    private void AddBullets()
    {
        string nameBall = "Ball";

        for (int i = 0; i < 50; i++)
        {
            GameObject newGameObj = Instantiate(PrefabBullet);
            newGameObj.name = nameBall;
            newGameObj.transform.parent = transform;
            newGameObj.transform.localPosition = Vector3.zero;
            newGameObj.transform.localScale = Vector3.one;

            Bullet newBullet = newGameObj.GetComponent<Bullet>();
            BulletList.Add(newBullet);

            newGameObj.SetActive(false);
        }
    }

    public void NotifyLandingFirstBullet(Transform landFirstBullet)
    {
        for (int i = 0; i < BulletList.Count; i++)
            BulletList[i].LandFirstBullet(landFirstBullet.localPosition);

        ++CountLandedBullet;

        Transform prevParent = PlayerTweenPos.transform.parent;
        Vector3 pos = PlayerTweenPos.transform.position;

        PlayerTweenPos.ResetToBeginning();
        PlayerTweenPos.transform.parent = landFirstBullet;
        PlayerTweenPos.transform.position = pos;
        PlayerTweenPos.from = PlayerTweenPos.transform.localPosition;
        PlayerTweenPos.to = new Vector3(120.0f, -20.0f, 0.0f);
        GamePlayManager.GetInstance.RecvLandFirstBullet();
        PlayerTweenPos.PlayForward();
    }

    public void changeParent(Transform parent) { PlayerTweenPos.transform.parent = parent; }

    IEnumerator FireBulletByInterval (Vector3 towards, int fireCount, float fireInterval)
    {
        float spd = GamePlayManager.BallSpd;
        int i = 0;

        while (i < fireCount)
        {
            --CountLandedBullet;
            BulletList[i].Move(towards, spd);
            yield return new WaitForSeconds(fireInterval);
            ++i;
        }
    }

    public void DeActivateAllBulletsGameObject ()
    {
        Function.SetAllGameObjectActiveSelfInPool(BulletList, false);
        Function.SetAllTransformParentToTarget(BulletList, (x => !x.gameObject.activeSelf), transform);
    }

    public void ActivateBulletOnlyOne ()
    {
        Bullet bullet = BulletList.Find(x => !x.gameObject.activeSelf);

        bullet.gameObject.SetActive(true);
        bullet.transform.parent = PlayerTweenPos.transform;
        bullet.transform.localPosition = new Vector3(-120.0f, 20.0f, 0.0f);
        bullet.transform.parent = transform;
        bullet.PropIsBulletLandBottomFirst = false;
    }
}
