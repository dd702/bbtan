﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class BrickManager : MonoBehaviour
{

    public UIGrid RefGridBricks = null;
    public TweenPosition RefGridBricksTweenPosition = null;
    public Brick PrefabBrickSquare = null;
    public Brick PrefabBrickTriangle = null;
    public ParticleSystem PrefabBrickBreakParticle = null;
    public Transform RefParticlesPanel = null;
    private List<Brick> ListSquareBricks = new List<Brick>();
    private List<Brick> ListTriangleBricks = new List<Brick>();
    private List<GameObject> ListEmptyBricks = new List<GameObject>();
    private List<UIGrid> ListGridBricksRow = new List<UIGrid>();
    private List<ParticleSystem> ListParticles = new List<ParticleSystem>();

    public List<UIGrid> PropListGridBricksRow { get { return ListGridBricksRow; } }
    public List<GameObject> PropListEmptyBrick { get { return ListEmptyBricks; } }

    public int CurrentRow { get; set; }

    public void AddBricks()
    {
        for (int i = 0; i < 50; ++i)
        {
            Brick brickSquare = Instantiate(PrefabBrickSquare);
            brickSquare.transform.parent = transform;
            brickSquare.transform.localScale = Vector3.one;
            brickSquare.gameObject.SetActive(false);
            ListSquareBricks.Add(brickSquare);

            Brick brickTriangle = Instantiate(PrefabBrickTriangle);
            brickTriangle.transform.parent = transform;
            brickTriangle.transform.localScale = Vector3.one;
            brickTriangle.gameObject.SetActive(false);
            ListTriangleBricks.Add(brickTriangle);
        }

        AddEmptyBrick();
    }

    public void AddEmptyBrick ()
    {
        string strNameEmpty = "EmptyBrick";

        for (int i = 0; i < 50; ++i)
        {
            GameObject emptyGo = new GameObject(strNameEmpty);
            emptyGo.transform.parent = transform;
            emptyGo.SetActive(false);
            ListEmptyBricks.Add(emptyGo);
        }
    }

    void Awake()
    {
        Init();
    }

    void Init()
    {
        for (int i = 0; i < 10; ++i)
        {
            ParticleSystem particleSystem = Instantiate(PrefabBrickBreakParticle);
            particleSystem.gameObject.SetActive(false);
            particleSystem.transform.parent = RefParticlesPanel;
            particleSystem.gameObject.layer = RefParticlesPanel.gameObject.layer;
            ListParticles.Add(particleSystem);
        }

        // Brick Grids Add To List;
        List<Transform> gridBricksRows = RefGridBricks.GetChildList();
        for (int i = 0; i < gridBricksRows.Count; ++i)
            ListGridBricksRow.Add(gridBricksRows[i].GetComponent<UIGrid>());

        //Bricks
        AddBricks();

        CurrentRow = -1;
    }

    public void DrawBrick()
    {
        int currentTurn = GamePlayManager.GetInstance.PropCurrentTurn;
        int maxDrawBricks = 7;
        int gridLastChildIdx = ListGridBricksRow.Count - 1;

        if (currentTurn < 15)
            maxDrawBricks = 4;

        int drawCount = Random.Range(1, maxDrawBricks);

        // SelectDrawedBricks
        for (int i = 0; i < drawCount; ++i)
            SelectDrawedBricks(currentTurn);

        // refresh old bricks state
        RefreshBricksStat();
    }

    void RefreshBricksStat ()
    {
        for (int i = 0; i < ListTriangleBricks.Count; ++i)
        {
            if (!ListTriangleBricks[i].gameObject.activeSelf)
                continue;

            ListTriangleBricks[i].SetHP(ListTriangleBricks[i].HP, false);
        }
            
        for (int i = 0; i < ListSquareBricks.Count; ++i)
        {
            if (!ListSquareBricks[i].gameObject.activeSelf)
                continue;

            ListSquareBricks[i].SetHP(ListSquareBricks[i].HP, false);
        }
            
    }

    // *It's just called by GamePlayManager
    public void AddCurrentTurn()
    {
        int currentTurn = GamePlayManager.GetInstance.PropCurrentTurn;
        CurrentRow = currentTurn % ListGridBricksRow.Count;
    }

    //What, Where In Brick
    void SelectDrawedBricks(int currentTurn)
    {
        int triOrSquare = Random.Range(0, 3);
        int gridChildLastIdx = ListGridBricksRow.Count - 1;
        if (triOrSquare == 0)
        {
            for (int i = 0; i < ListTriangleBricks.Count; ++i)
            {
                if (ListTriangleBricks[i].gameObject.activeSelf) continue;

                ListTriangleBricks[i].gameObject.SetActive(true);
                ListTriangleBricks[i].transform.localScale = Vector3.one;
                ListTriangleBricks[i].transform.eulerAngles = new Vector3(0.0f, 0.0f, Random.Range(0, 4) * 90.0f);
                ListTriangleBricks[i].TweenColorLabel.transform.rotation = Quaternion.Euler(Vector3.zero);

                ListTriangleBricks[i].transform.parent = ListGridBricksRow[gridChildLastIdx - CurrentRow].transform;
                ListTriangleBricks[i].gameObject.layer = ListTriangleBricks[i].transform.parent.gameObject.layer;
                ListTriangleBricks[i].SetHP(currentTurn + 1, false);
                break;
            }
        }
        else
        {
            for (int i = 0; i < ListSquareBricks.Count; ++i)
            {
                if (ListSquareBricks[i].gameObject.activeSelf) continue;

                ListSquareBricks[i].gameObject.SetActive(true);
                ListSquareBricks[i].transform.localScale = Vector3.one;
                ListSquareBricks[i].transform.parent = ListGridBricksRow[gridChildLastIdx - CurrentRow].transform;
                ListSquareBricks[i].gameObject.layer = ListSquareBricks[i].transform.parent.gameObject.layer;
                ListSquareBricks[i].SetHP(currentTurn + 1, false);
                break;
            }
        }
    }

    public void MoveBrickBreakParticle(Transform parent)
    {
        for (int i = 0; i < ListParticles.Count; ++i)
        {
            if (ListParticles[i].gameObject.activeSelf) continue;

            ListParticles[i].gameObject.SetActive(true);
            ListParticles[i].transform.position = parent.position;
            ListParticles[i].transform.localScale = new Vector3(0.08f, 0.08f, 0.08f);
            ListParticles[i].Play();
            StartCoroutine(CountParticleLifeTime(ListParticles[i]));

            break;
        }
    }

    IEnumerator CountParticleLifeTime(ParticleSystem targetParticle)
    {
        yield return new WaitForSeconds(targetParticle.startLifetime);

        targetParticle.Stop();
        targetParticle.gameObject.SetActive(false);
    }

    void RepositionGrids()
    {
        int currentTurn = GamePlayManager.GetInstance.PropCurrentTurn;

        if (currentTurn > 7)
        {
            RefGridBricks.GetChild(ListGridBricksRow.Count - 1).SetAsFirstSibling();
            List<Transform> childList = RefGridBricks.GetChild(0).GetComponent<UIGrid>().GetChildList();

            for (int i = 0; i < childList.Count; ++i)
            {
                childList[i].transform.parent = transform;
                childList[i].gameObject.SetActive(false);
            }

            RefGridBricks.GetChild(0).gameObject.SetActive(false);
            RefGridBricks.GetChild(1).gameObject.SetActive(true);
            RefGridBricks.GetChild(2).gameObject.SetActive(true);
        }

        for (int i = 0; i < ListGridBricksRow.Count; ++i)
            ListGridBricksRow[i].Reposition();

        RefGridBricks.Reposition();
    }

    public void ChangeToEmptyBrick(Transform target)
    {
        GameObject emptyBrick = Function.CheckItemPool(ListEmptyBricks, (x => !x.gameObject.activeSelf), AddEmptyBrick);

        emptyBrick.transform.parent = target.parent;
        emptyBrick.transform.SetSiblingIndex(target.GetSiblingIndex());
        emptyBrick.gameObject.SetActive(true);

        target.gameObject.SetActive(false);
        target.parent = transform;
    }

    public void CheckGameOverCondition()
    {
        int currentTurn = GamePlayManager.GetInstance.PropCurrentTurn;

        if (currentTurn < 7)
            return;

        int lastRowIdx = (currentTurn == 7) ? ListGridBricksRow.Count - 1 : ListGridBricksRow.Count - 2;

        Transform lastRow = RefGridBricks.GetChild(lastRowIdx);

        if (lastRow.GetComponentInChildren<Brick>() != null)
        {
            if (GamePlayManager.GetInstance.IsOneMoreChance)
                Application.Quit();

            GamePlayManager.GetInstance.GameEnd();
        }
            
    }

    public void RepositionBricks()
    {
        int gridLastChildIdx = ListGridBricksRow.Count - 1;
        List<Transform> listBricksTransform = ListGridBricksRow[gridLastChildIdx - CurrentRow].GetChildList();

        int lastIdxBricks = listBricksTransform.Count - 1;

        // Rand Bricks & Items
        for (int i = 0; i < 100; ++i)
        {
            int rand1st = Random.Range(0, lastIdxBricks);
            int rand2nd = Random.Range(0, lastIdxBricks);

            try
            {
                listBricksTransform[rand1st].SetSiblingIndex(rand2nd);
            }
            catch (System.Exception e)
            {
                Debug.Log(rand1st.ToString() + ", " + rand2nd.ToString());
            }

        }

        // Reposition Grids
        RepositionGrids();
    }

    public void FillLeftBrickToEmptyBrick()
    {
        int gridChildLastIdx = ListGridBricksRow.Count - 1;
        int drawedCount = ListGridBricksRow[gridChildLastIdx - CurrentRow].transform.childCount;

        for (int i = drawedCount; i < 7; ++i)
        {
            for (int j = 0; j < ListEmptyBricks.Count; ++j)
            {
                if (ListEmptyBricks[j].activeSelf) continue;

                ListEmptyBricks[j].SetActive(true);
                ListEmptyBricks[j].transform.parent = ListGridBricksRow[gridChildLastIdx - CurrentRow].transform;
                break;
            }
        }
    }

    public void DeActivateAllBricks()
    {
        Function.SetAllGameObjectActiveSelfInPool(ListTriangleBricks, false);
        Function.SetAllTransformParentToTarget(ListTriangleBricks, (x => !x.gameObject.activeSelf), transform);

        Function.SetAllGameObjectActiveSelfInPool(ListSquareBricks, false);
        Function.SetAllTransformParentToTarget(ListSquareBricks, (x => !x.gameObject.activeSelf), transform);

        Function.SetAllGameObjectActiveSelfInPool(ListEmptyBricks, false);
        Function.SetAllTransformParentToTarget(ListEmptyBricks, (x => !x.gameObject.activeSelf), transform);

        Function.SetAllGameObjectActiveSelfInPool(ListParticles, false);
        Function.SetAllTransformParentToTarget(ListParticles, (x => !x.gameObject.activeSelf), transform);
    }

    public void RepositionGridsSibling ()
    {
        for (int i = 0; i < ListGridBricksRow.Count; ++i)
        {
            ListGridBricksRow[i].transform.SetAsLastSibling();
            ListGridBricksRow[i].gameObject.SetActive(true);
        }
        ListGridBricksRow[0].gameObject.SetActive(false);
        ListGridBricksRow[1].gameObject.SetActive(false);
    }
}
