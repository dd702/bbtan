﻿using UnityEngine;
using System.Collections;

public enum eGameState
{
    eCanShoot,
    eCantShoot
}

public class GamePlayManager : MonoBehaviour {

    private const string StrCoin = "Coin";
    private const string StrTopScore = "TopScore";

    private const string StrFirstStart = "FirstStart";
    private const string StrReStart = "ReStart";
    private const string StrGameStart = "GameStart";
    private const string StrGameEnd = "GameEnd";
    private const string StrOneMoreChance = "OneMoreChance";
    private const string StrButtonClick = "ButtonClick";

    public const string StrPlayPage = "https://play.google.com/store/apps/details?id=com.crater.bbtan&hl=ko";

    public const float BallSpd = 1.5f;

    private static GamePlayManager Instance = null;
    public static GamePlayManager GetInstance
    {
        get
        {
            if (Instance == null)
                Instance = GameObject.Find("GamePlayManager").GetComponent<GamePlayManager>();

            return Instance;
        }
    }

    public BrickManager RefBrickManager = null;
    public UILabel LabelScore = null;
    public UILabel LabelTopScore = null;
    public UILabel LabelCoin = null;
    public SoundManager RefSoundManager = null;
    public ItemManager RefItemManager = null;
    public PlayerControl PlayerControl = null;
    public BulletManager RefBulletManager = null;
    public TweenScale TweenScaleBallCount = null;
    public Animator AnimatorStartUI = null;
    public GameObject PauseGame = null;
    public UnityAds RefUnityAds = null;
    public UISprite SoundButton = null;

    [HideInInspector] public eGameState GameState = eGameState.eCanShoot;

    private UILabel LabelBallCount = null;
    public string PropLabelBallCount
    {
        set
        {
            LabelBallCount.text = string.Format("x{0}", value);
            if (RefBulletManager.CountLandedBullet == 0)
            {
                //TweenScaleBallCount.ResetToBeginning();
                TweenScaleBallCount.PlayForward();
            }
        }
    }

    private int nFireCount = 0;
    public int FireCount
    {
        get { return nFireCount; }
        set { nFireCount = value; }
    }

    public int AddFireCount  { get; set; }

    private int CurrentTurn = -1;
    public int PropCurrentTurn
    {
        get { return CurrentTurn; }
        set { CurrentTurn = value; }
    }
    private int Score = 0;
    public int PropScore
    {
        get { return Score; }
        set
        {
            Score = value;
            LabelScore.text = value.ToString();

            if (PropTopScore < value)
                PropTopScore = value;
        }
    }

    private int TopScore = 0;
    public int PropTopScore
    {
        get { return TopScore; }
        set
        {
            TopScore = value;
            LabelTopScore.text = value.ToString();
            PlayerPrefs.SetInt(StrTopScore, value);
        }
    }

    private int Coin = 0;
    public int PropCoin
    {
        get { return Coin; }
        set
        {
            LabelCoin.text = string.Format("{0}", value);
            Coin = value;
        }
    }

    [HideInInspector] public bool IsOneMoreChance = false;


    void Awake()
    {
#if UNITY_ANDROID || UNITY_IOS
        Screen.orientation = ScreenOrientation.Portrait;
#endif

        Init();

        AnimatorStartUI.SetTrigger(StrFirstStart);
        RefUnityAds.SetActionAdsFinished(ActionAdsFinished);
    }
    
    void Init ()
    {
        SetRandomSeed();
        PropScore = 0;
        TopScore = 0;
        LabelBallCount = PlayerControl.GetComponentInChildren<UILabel>();
        AddFireCount = 0;
        FireCount = 1;
        RefBulletManager.CountLandedBullet = 1;
        PropCoin = Function.GetValue(StrCoin);
        PropTopScore = Function.GetValue(StrTopScore);
        PropCurrentTurn = -1;
    }

    void SetRandomSeed ()
    {
        if (PlayerPrefs.HasKey("RandSeed"))
        {
            Random.seed = PlayerPrefs.GetInt("RandSeed");
            PlayerPrefs.SetInt("RandSeed", Random.Range(0, 65536));
        }
        else
            PlayerPrefs.SetInt("RandSeed", Random.Range(0, 65536));
    }


    void TweenBricksDownward ()
    {
        if (CurrentTurn > 7) return;

        RefBrickManager.RefGridBricksTweenPosition.ResetToBeginning();
        RefBrickManager.RefGridBricksTweenPosition.from = new Vector3(0.0f, (CurrentTurn - 1) * -155.0f - 75.0f, 0.0f);
        RefBrickManager.RefGridBricksTweenPosition.to = new Vector3(0.0f, CurrentTurn * -155.0f - 75.0f, 0.0f);
        RefBrickManager.RefGridBricksTweenPosition.PlayForward();
    }

    public void CallNextTurn ()
    {
        // AddTurn
        ++CurrentTurn;
        RefBrickManager.AddCurrentTurn();
        ++PropScore;
        GameState = eGameState.eCanShoot;

        // Check GameOver Condition LastBricksRow
        RefBrickManager.CheckGameOverCondition();

        // AddBallCount
        FireCount += AddFireCount;
        RefBulletManager.CountLandedBullet += AddFireCount;
        AddFireCount = 0;

        // Change OneTurnUsedItem to EmptyBrick
        RefItemManager.CheckOneTurnUsed();

        // Makeup Bricks & Item
        RefBrickManager.DrawBrick();
        RefItemManager.DrawItem();

        // Fill EmptyBlock
        RefBrickManager.FillLeftBrickToEmptyBrick();

        // reposition grids & bricks & items
        RefBrickManager.RepositionBricks();
        
        // Move Downard Bricks
        TweenBricksDownward ();
    }

    void Update ()
    {
        if (!Application.isEditor)
            return;

        if (Input.GetKeyDown(KeyCode.F2))
        {
            Time.timeScale *= 0.5f;
            Debug.Log(Time.timeScale.ToString());
        }
            

        else if (Input.GetKeyDown(KeyCode.F1))
            CallNextTurn();

        else if (Input.GetKeyDown(KeyCode.F3))
        {
            ++FireCount;
            ++RefBulletManager.CountLandedBullet;
        }
    }

    public void MoveBrickBreakParticle (Transform transform) { RefBrickManager.MoveBrickBreakParticle(transform); }
    public void RecvLandFirstBullet () { TweenScaleBallCount.PlayReverse(); }

    public void OpenGooglePlayPage () { Application.OpenURL(StrPlayPage); }
    public void OnButtonPlayGame () { AnimatorStartUI.SetTrigger(StrGameStart); }
    public void OnBtnGameRestart ()
    {
        RefSoundManager.PlaySoundOneShot(StrButtonClick);
        AnimatorStartUI.SetTrigger(StrReStart);
        RefBulletManager.DeActivateAllBulletsGameObject();
        RefBrickManager.DeActivateAllBricks();
        RefBrickManager.RepositionGridsSibling();
        RefItemManager.DeActivateAllItems();
        PauseGame.SetActive(false);
        Init();
    }

    public void GameEnd ()
    {
        PlayerPrefs.SetInt(StrCoin, PropCoin);
        PlayerPrefs.SetInt(StrTopScore, PropTopScore);

        RefSoundManager.PlaySoundOneShot(SoundManager.StrGameEnd);
        GameEndAnimation();
    }
    public void GameEndAnimation () { AnimatorStartUI.SetTrigger(StrGameEnd); }
    public void GameStart () { RefBulletManager.ActivateBulletOnlyOne(); }

    public void OnBtnClickPause ()
    {
        RefSoundManager.PlaySoundOneShot(StrButtonClick);
        PauseGame.gameObject.SetActive(true);
        PauseGame.transform.GetChild(4).gameObject.SetActive(true);
        PauseGame.transform.GetChild(5).gameObject.SetActive(false);
    }

    public void OnBtnClickResume ()
    {
        RefSoundManager.PlaySoundOneShot(StrButtonClick);
        PauseGame.gameObject.SetActive(false);
    }

    public void OnBtnClickQuestion ()
    {
        RefSoundManager.PlaySoundOneShot(StrButtonClick);
        PauseGame.gameObject.SetActive(true);
        PauseGame.transform.GetChild(4).gameObject.SetActive(false);
        PauseGame.transform.GetChild(5).gameObject.SetActive(true);
    }

    public void OnBtnOneMoreChance ()
    {
        RefUnityAds.ShowRewardedAd();
        IsOneMoreChance = true;
    }

    private void ActionAdsFinished ()
    {
        int lastRow = RefBrickManager.PropListGridBricksRow[0].transform.parent.GetChildCount() - 1;
        Brick[] bricks = RefBrickManager.PropListGridBricksRow[0].transform.parent.GetChild(lastRow).GetComponentsInChildren<Brick>();

        for (int i = 0; i < bricks.Length; ++i)
            bricks[i].SetHP(0, false);

        AnimatorStartUI.SetTrigger(StrOneMoreChance);
    }

    public void SwitchSoundButton ()
    {
        RefSoundManager.SwitchAudioMute();
        RefSoundManager.PlaySoundOneShot(StrButtonClick);
        SoundButton.GetComponent<UIButton>().normalSprite = (RefSoundManager.SoundPlayer.mute) ? "no_sound_button" : "sound_button";
        SoundButton.MakePixelPerfect();
    }
}
