﻿using UnityEngine;
using System;
using System.Reflection;
using System.Collections.Generic;



public class Function
{

    public static void Shuffle<T>(List<T> randList)
    {
        int randCount = randList.Count * 3;
        int randMax = randList.Count;

        while (randCount > 1)
        {
            T temp = default(T);

            int i = UnityEngine.Random.Range(0, randMax);
            int j = UnityEngine.Random.Range(0, randMax);

            try
            {
                temp = randList[i];
                randList[i] = randList[j];
                randList[j] = temp;
            }
            catch
            {
                Debug.Log(i.ToString() + " ," + j.ToString());
            }

            --randCount;
        }
    }

    public static T CheckItemPool<T>(List<T> list, Predicate<T> predicate, Action nullExecution)
    {
        T defaultValue = default(T);
        T findBehaviour = list.Find(predicate);

        if (findBehaviour == null)
            nullExecution();

        findBehaviour = list.Find(predicate);
        return findBehaviour;
    }

    public static void SetAllGameObjectActiveSelfInPool<T>(List<T> list, bool settingActiveSelf)
        where T : Component
    {
        T t = list.Find(x => x.gameObject.activeSelf != settingActiveSelf);

        while (t != null)
        {
            t.gameObject.SetActive(settingActiveSelf);
            t = list.Find(x => x.gameObject.activeSelf != settingActiveSelf);
        }
    }

    public static void SetAllTransformParentToTarget(List<GameObject> list, Predicate<GameObject> predicate, Transform target)
    {
        List<GameObject> t = list.FindAll(predicate);

        for (int i = 0; i < t.Count; ++i)
            t[i].transform.parent = target;
    }

    public static void SetAllTransformParentToTarget<T>(List<T> list, Predicate<T> predicate, Transform target)
        where T : Component
    {
        List<T> t = list.FindAll(predicate);

        for (int i = 0; i < t.Count; ++i)
            t[i].transform.parent = target;
    }

    public static void SetAllGameObjectActiveSelfInPool(List<GameObject> list, bool settingActiveSelf)
    {
        GameObject go = list.Find(x => x.gameObject.activeSelf != settingActiveSelf);

        while (go != null)
        {
            go.gameObject.SetActive(settingActiveSelf);
            go = list.Find(x => x.gameObject.activeSelf != settingActiveSelf);
        }
    }

    public static int GetItemIndexPool<T>(List<T> list, Predicate<T> predicate, Action nullExecution) { return list.FindIndex(predicate); }

    public static int GetValue (string key)
    {
        int value = 0;
        if (PlayerPrefs.HasKey(key))
            value = PlayerPrefs.GetInt(key);

        return value;
    }
}
