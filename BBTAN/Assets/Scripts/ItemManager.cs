﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

public enum ItemCategory
{
    eAddBullet,
    eChangeDirection,
    eLaserHorizontal,
    eLaserVertical,
    eCoin
}

public class ItemManager : MonoBehaviour
{
    private const int PreAddItemCount = 10;
    public const string StrAnimationTrigger = "Trigger";
    public const string StrLaser = "Laser";
    public static Color AddCoinColor = new Color(255.0f, 255.0f, 0.0f);
    public static Color AddBulletColor = new Color(218.0f, 235.0f, 104.0f);


    public Item PrefabAddBullet = null;
    public Item PrefabChangeDirection = null;
    public Item PrefabLaserHorizontal = null;
    public Item PrefabLaserVertical = null;
    public Item PrefabCoin = null;
    public UILabel PrefabLabelAdded = null;

    [HideInInspector] public List<UILabel> ListLabelAdded = new List<UILabel>();
    [HideInInspector] public List<TweenColor> ListLabelAddedTweenColor = new List<TweenColor>();
    [HideInInspector] public List<TweenPosition> ListLabelAddedTweenPosition = new List<TweenPosition>();

    private List<Item> ListAddBullet        = new List<Item>();
    private List<Item> ListChangeDirection  = new List<Item>();
    private List<Item> ListLaserHorizontal  = new List<Item>();
    private List<Item> ListLaserVertical    = new List<Item>();
    private List<Item> ListCoin             = new List<Item>();
    private Dictionary<ItemCategory, List<Item>> DicItemList = new Dictionary<ItemCategory, List<Item>>();

    public List<Item> GetItemList (ItemCategory key) { return DicItemList[key]; }

    public Animator PrefabLaser = null;
    private List<Animator> ListLaser = new List<Animator>();


    void Awake ()
    {
        Init();
    }

    void Init ()
    {
        AddItemListToDictionary();
        AddItemToList();
        AddAddedLabelToList();
    }

    void AddItemListToDictionary ()
    {
        DicItemList.Add(ItemCategory.eAddBullet, ListAddBullet);
        DicItemList.Add(ItemCategory.eChangeDirection, ListChangeDirection);
        DicItemList.Add(ItemCategory.eLaserHorizontal, ListLaserHorizontal);
        DicItemList.Add(ItemCategory.eLaserVertical, ListLaserVertical);
        DicItemList.Add(ItemCategory.eCoin, ListCoin);
    }

    void AddItemToList()
    {
        for (int i = 0; i < PreAddItemCount; ++i)
        {
            Item itemAddBullet          = Instantiate(PrefabAddBullet);
            Item itemChangeDirection    = Instantiate(PrefabChangeDirection);
            Item itemLaserHorizontal    = Instantiate(PrefabLaserHorizontal);
            Item itemLaserVertical      = Instantiate(PrefabLaserVertical);
            Item itemCoin               = Instantiate(PrefabCoin);

            itemAddBullet.gameObject.SetActive(false);
            itemChangeDirection.gameObject.SetActive(false);
            itemLaserHorizontal.gameObject.SetActive(false);
            itemLaserVertical.gameObject.SetActive(false);
            itemCoin.gameObject.SetActive(false);

            itemAddBullet.transform.parent = transform;
            itemChangeDirection.transform.parent = transform;
            itemLaserHorizontal.transform.parent = transform;
            itemLaserVertical.transform.parent = transform;
            itemCoin.transform.parent = transform;

            ListAddBullet.Add(itemAddBullet);
            ListChangeDirection.Add(itemChangeDirection);
            ListLaserHorizontal.Add(itemLaserHorizontal);
            ListLaserVertical.Add(itemLaserVertical);
            ListCoin.Add(itemCoin);
        }

        AddLaser();
    }

    void AddLaser()
    {
        for (int i = 0; i < 20; ++i)
        {
            Animator laser = Instantiate(PrefabLaser);

            laser.gameObject.name = "Laser";
            laser.gameObject.SetActive(false);
            laser.transform.parent = transform;
            ListLaser.Add(laser);
        }
    }

    public void AddAddedLabelToList()
    {
        for (int i = 0; i < 10; ++i)
        {
            UILabel labelAdded = Instantiate(PrefabLabelAdded);

            labelAdded.gameObject.SetActive(false);
            labelAdded.transform.parent = transform;
            labelAdded.transform.localScale = Vector3.one;
            labelAdded.transform.localPosition = Vector3.zero;

            ListLabelAdded.Add(labelAdded);
            ListLabelAddedTweenColor.Add(labelAdded.GetComponent<TweenColor>());
            ListLabelAddedTweenPosition.Add(labelAdded.GetComponent<TweenPosition>());
        }
    }

    void SelectDrawedItem()
    {
        List<UIGrid> listGridBrickRows = GamePlayManager.GetInstance.RefBrickManager.PropListGridBricksRow;

        int gridChildLastIdx = listGridBrickRows.Count - 1;
        int currentRow = GamePlayManager.GetInstance.RefBrickManager.CurrentRow;
        Transform currentRowTransform = listGridBrickRows[gridChildLastIdx - currentRow].transform;

        //AddBullet (0)
        List<Item> listAddBullet = DicItemList[ItemCategory.eAddBullet];
        Item deActItemAddBullet = Function.CheckItemPool(listAddBullet, (x => !x.gameObject.activeSelf), AddItemToList);

        deActItemAddBullet.gameObject.SetActive(true);
        deActItemAddBullet.transform.parent = currentRowTransform;
        deActItemAddBullet.transform.localScale = Vector3.one;

        //Shuffle
        List<int> forRandItemArray = Enumerable.Range((int)ItemCategory.eChangeDirection, (int)ItemCategory.eCoin).ToList();
        int currentRowFilledCount = listGridBrickRows[gridChildLastIdx - currentRow].transform.childCount;
        int leftToField = 7 - currentRowFilledCount;
        Function.Shuffle(forRandItemArray);

        //Category (1~4)
        int chanceAllocateItem = Random.Range(0, leftToField + 1);
        int i = 0;

        while (chanceAllocateItem > 0)
        {
            if (Random.Range(0, 2) == 0)
            {
                List<Item> selectItemList = DicItemList[(ItemCategory)forRandItemArray[i]];
                Item selectItem = Function.CheckItemPool(selectItemList, (x => !x.gameObject.activeSelf), AddItemToList);

                selectItem.gameObject.SetActive(true);
                selectItem.transform.parent = currentRowTransform;
                selectItem.transform.localScale = Vector3.one;

                ++i;
            }

            --chanceAllocateItem;
        }
    }

    public void DrawItem ()
    {
        SelectDrawedItem();
    }

    public Animator GetLaser ()
    {
        Animator laser = Function.CheckItemPool(ListLaser, (x => !x.gameObject.activeSelf), AddLaser);

        return laser;
    }

    void ChangeOneTurnUsedToEmptyBrick(ItemOneTurnDisposable target)
    {
        GameObject emptyBrick = Function.CheckItemPool(GamePlayManager.GetInstance.RefBrickManager.PropListEmptyBrick,
            (x => !x.gameObject.activeSelf), GamePlayManager.GetInstance.RefBrickManager.AddEmptyBrick);

        emptyBrick.transform.parent = target.transform.parent;
        emptyBrick.transform.SetSiblingIndex(target.transform.GetSiblingIndex());
        emptyBrick.gameObject.SetActive(true);

        target.gameObject.SetActive(false);
        target.transform.parent = transform;
        target.IsOneTimeUsed = false;
    }

    public void ChangeToEmptyBrick(Transform target)
    {
        GameObject emptyBrick =
            Function.CheckItemPool(GamePlayManager.GetInstance.RefBrickManager.PropListEmptyBrick,
            (x => !x.gameObject.activeSelf), GamePlayManager.GetInstance.RefBrickManager.AddEmptyBrick);

        emptyBrick.transform.parent = target.parent;
        emptyBrick.transform.SetSiblingIndex(target.GetSiblingIndex());
        emptyBrick.gameObject.SetActive(true);

        target.gameObject.SetActive(false);
        target.parent = transform;
    }

    public void CheckOneTurnUsed ()
    {
        for (int i = 0; i < ListChangeDirection.Count; ++i)
        {
            if (!ListChangeDirection[i].gameObject.activeSelf) continue;

            ItemOneTurnDisposable disposable = ListChangeDirection[i] as ItemOneTurnDisposable;

            if (disposable.IsOneTimeUsed)
                ChangeOneTurnUsedToEmptyBrick(disposable);
        }

        for (int i = 0; i < ListLaserHorizontal.Count; ++i)
        {
            if (!ListLaserHorizontal[i].gameObject.activeSelf) continue;

            ItemOneTurnDisposable disposable = ListLaserHorizontal[i] as ItemOneTurnDisposable;

            if (disposable.IsOneTimeUsed)
                ChangeOneTurnUsedToEmptyBrick(disposable);
        }

        for (int i = 0; i < ListLaserVertical.Count; ++i)
        {
            if (!ListLaserVertical[i].gameObject.activeSelf) continue;

            ItemOneTurnDisposable disposable = ListLaserVertical[i] as ItemOneTurnDisposable;

            if (disposable.IsOneTimeUsed)
                ChangeOneTurnUsedToEmptyBrick(disposable);
        }
    }

    public void SettingAddedLabel (Transform target, Color color)
    {
        int idx = Function.GetItemIndexPool(ListLabelAdded, (x => !x.gameObject.activeSelf), AddAddedLabelToList);

        ListLabelAdded[idx].gameObject.SetActive(true);
        ListLabelAdded[idx].transform.position = target.position;
        ListLabelAdded[idx].color = color;

        ListLabelAddedTweenColor[idx].ResetToBeginning();
        ListLabelAddedTweenColor[idx].PlayForward();

        ListLabelAddedTweenPosition[idx].ResetToBeginning();
        ListLabelAddedTweenPosition[idx].from = ListLabelAddedTweenPosition[idx].transform.worldToLocalMatrix.MultiplyVector(target.position);
        ListLabelAddedTweenPosition[idx].to = ListLabelAddedTweenPosition[idx].from + new Vector3(0.0f, 50.0f, 0.0f);
        ListLabelAddedTweenPosition[idx].PlayForward();

        StartCoroutine(AfterTween(ListLabelAddedTweenColor[idx]));
    }

    IEnumerator AfterTween (UITweener tweener)
    {
        yield return new WaitForSeconds(tweener.duration);

        tweener.gameObject.SetActive(false);
    }

    public void DeActivateAllItems ()
    {
        Function.SetAllGameObjectActiveSelfInPool(ListAddBullet, false);
        Function.SetAllTransformParentToTarget(ListAddBullet, (x => !x.gameObject.activeSelf), transform);

        Function.SetAllGameObjectActiveSelfInPool(ListChangeDirection, false);
        Function.SetAllTransformParentToTarget(ListChangeDirection, (x => !x.gameObject.activeSelf), transform);

        Function.SetAllGameObjectActiveSelfInPool(ListLaserHorizontal, false);
        Function.SetAllTransformParentToTarget(ListLaserHorizontal, (x => !x.gameObject.activeSelf), transform);

        Function.SetAllGameObjectActiveSelfInPool(ListLaserVertical, false);
        Function.SetAllTransformParentToTarget(ListLaserVertical, (x => !x.gameObject.activeSelf), transform);

        Function.SetAllGameObjectActiveSelfInPool(ListCoin, false);
        Function.SetAllTransformParentToTarget(ListCoin, (x => !x.gameObject.activeSelf), transform);
    }
}
