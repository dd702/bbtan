﻿using UnityEngine;
using System.Text;

public class PlayerControl : MonoBehaviour
{

    private const int MouseButtonL = 0;                     // Left Mouse Button Constants.
    private bool IsCanShootState = false;                   // Possibility of Shooting Bullet
    private const float TriggerShootBallDistance = 50.0f;   // When LeftMouseButton Up, MinDistance of Shooting Ball.

    private Vector3 MouseButtonDownPos;                 // When Left Mouse Button Clicked, Catch Screen Pos.

    private BulletManager RefBulletManager = null;
    public TweenScale TweenScaleTouchedSpot = null;
    public TweenScale TweenScaleHelpArrow = null;


    void Awake()
    {
        RefBulletManager = GameObject.FindGameObjectWithTag("BulletManager").GetComponent<BulletManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (GamePlayManager.GetInstance.GameState == eGameState.eCantShoot)
            return;

#if UNITY_ANDROID || UNITY_IOS
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            if (touch.phase == TouchPhase.Began)
            {
                MouseButtonDownPos = touch.position;
                TweenScaleTouchedSpot.ResetToBeginning();
                TweenScaleHelpArrow.ResetToBeginning();
            }
            else if (touch.phase == TouchPhase.Ended)
            {
                if (IsCanShootState)
                {
                    RefBulletManager.Fire(MouseButtonDownPos - new Vector3(touch.position.x, touch.position.y),
                        GamePlayManager.GetInstance.FireCount, 0.1f);
                    ValidateFireBullet(false);
                }
            }
            else if (touch.phase == TouchPhase.Moved)
            {
                float distDragAndDrop = Vector3.Distance(MouseButtonDownPos, Input.mousePosition);

                if (TriggerShootBallDistance < distDragAndDrop)
                {
                    if ((MouseButtonDownPos - Input.mousePosition).normalized.y > 0.2f)
                        ValidateFireBullet(true);
                    else
                        ValidateFireBullet(false);
                }
                else
                    ValidateFireBullet(false);
            }
        }
#elif UNITY_EDITOR || UNITY_STANDALONE
        if (Input.GetMouseButtonDown(MouseButtonL))
        {
            MouseButtonDownPos = Input.mousePosition;
            TweenScaleTouchedSpot.ResetToBeginning();
            TweenScaleHelpArrow.ResetToBeginning();
        }

        else if (Input.GetMouseButtonUp(MouseButtonL))
        {
            if (IsCanShootState)
            {
                RefBulletManager.Fire(MouseButtonDownPos - Input.mousePosition, GamePlayManager.GetInstance.FireCount, 0.1f);
                ValidateFireBullet(false);
            }
        }
        else if (Input.GetMouseButton(MouseButtonL))
        {
            float distDragAndDrop = Vector3.Distance(MouseButtonDownPos, Input.mousePosition);

            if (TriggerShootBallDistance < distDragAndDrop)
            {
                if ((MouseButtonDownPos - Input.mousePosition).normalized.y > 0.2f)
                    ValidateFireBullet(true);
                else
                    ValidateFireBullet(false);
            }
            else
                ValidateFireBullet(false);
        }
#endif
    }

    void ValidateFireBullet(bool isCanShoot)
    {
        if (isCanShoot)
        {
            TweenScaleTouchedSpot.transform.localScale = Vector3.one;
            TweenScaleTouchedSpot.transform.localPosition = TweenScaleTouchedSpot.transform.worldToLocalMatrix.MultiplyVector(Camera.main.ScreenToWorldPoint(MouseButtonDownPos));
            TweenScaleTouchedSpot.PlayForward();

            TweenScaleHelpArrow.transform.localScale = Vector3.one;
            TweenScaleHelpArrow.transform.localRotation = Quaternion.FromToRotation(Vector3.left, (MouseButtonDownPos - Input.mousePosition).normalized);
            TweenScaleHelpArrow.PlayForward();

            IsCanShootState = isCanShoot;
        }
        else
        {
            TweenScaleTouchedSpot.PlayReverse();
            TweenScaleHelpArrow.PlayReverse();
            IsCanShootState = isCanShoot;
        }
    }

    
}
