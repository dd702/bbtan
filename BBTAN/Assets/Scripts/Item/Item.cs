﻿using UnityEngine;
using System.Collections;
using System;

public interface ItemEffect
{
    void ActivateItemEffect(Collider2D collision);
}

public class Item : MonoBehaviour, ItemEffect
{
    private TweenScale TweenScaleItem = null;
    private const string StrBall = "Ball";

    protected void Awake ()
    {
        Init();
    }

    protected virtual void Init ()
    {
        TweenScaleItem = GetComponent<TweenScale>();
    }

    protected void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == StrBall)
        {
            TweenScaleItem.PlayForward();
            StartCoroutine(PlayBackTweenScale());

            ActivateItemEffect(collision);
        }   
    }

    public virtual void ActivateItemEffect(Collider2D collision) { }

    IEnumerator PlayBackTweenScale()
    {
        yield return new WaitForSeconds(TweenScaleItem.duration);
        TweenScaleItem.PlayReverse();
    }
}
