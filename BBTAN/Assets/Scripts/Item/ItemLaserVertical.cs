﻿using UnityEngine;
using System.Collections;

public class ItemLaserVertical : ItemOneTurnDisposable
{
    public AnimationClip LaserAnimationClip = null;

    public override void ActivateItemEffect(Collider2D collision)
    {
        Animator laser = GamePlayManager.GetInstance.RefItemManager.GetLaser();

        laser.gameObject.SetActive(true);
        laser.transform.parent = transform;
        laser.transform.localPosition = Vector3.zero;
        laser.transform.rotation = Quaternion.Euler(0.0f, 0.0f, 90.0f);
        GamePlayManager.GetInstance.RefSoundManager.PlaySoundOneShot(SoundManager.StrLaser);

        laser.SetTrigger(ItemManager.StrAnimationTrigger);

        IsOneTimeUsed = true;

        StartCoroutine(AfterAnimatonEvent(laser));
    }

    IEnumerator AfterAnimatonEvent(Animator laser)
    {
        yield return new WaitForSeconds(LaserAnimationClip.averageDuration);

        laser.gameObject.SetActive(false);
        laser.transform.parent = GamePlayManager.GetInstance.RefItemManager.transform;
    }
}