﻿using UnityEngine;
using System.Collections;

public class ItemCoin : Item {

    public override void ActivateItemEffect(Collider2D collision)
    {
        GamePlayManager.GetInstance.RefSoundManager.PlaySoundOneShot(SoundManager.StrCoin);

        GamePlayManager.GetInstance.RefItemManager.ChangeToEmptyBrick(transform);

        GamePlayManager.GetInstance.RefItemManager.SettingAddedLabel(transform, ItemManager.AddCoinColor);

        GamePlayManager.GetInstance.PropCoin = GamePlayManager.GetInstance.PropCoin + 1;
    }
}
