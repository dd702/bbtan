﻿using UnityEngine;
using System.Collections;

public class ItemOneTurnDisposable : Item {

    public bool IsOneTimeUsed = false;

    public void UseItem () { IsOneTimeUsed = true; }
}