﻿using UnityEngine;
using System.Collections;

public class ItemAddBullet : Item, ItemEffect
{
    public override void ActivateItemEffect(Collider2D collision)
    {
        GamePlayManager.GetInstance.RefSoundManager.PlaySoundOneShot(SoundManager.StrAddBullet);

        GamePlayManager.GetInstance.RefItemManager.ChangeToEmptyBrick(transform);

        GamePlayManager.GetInstance.RefItemManager.SettingAddedLabel(transform, ItemManager.AddBulletColor);

        ++GamePlayManager.GetInstance.AddFireCount;
    }
}
