﻿using UnityEngine;
using System.Collections;

public class ItemLaserHorizontal : ItemOneTurnDisposable
{
    public AnimationClip LaserAnimationClip = null;

    public override void ActivateItemEffect(Collider2D collision)
    {
        Animator laser = GamePlayManager.GetInstance.RefItemManager.GetLaser();

        laser.gameObject.SetActive(true);
        laser.transform.parent = transform;
        laser.transform.localPosition = Vector3.zero;
        laser.transform.rotation = Quaternion.EulerAngles(Vector3.zero);

        laser.SetTrigger(ItemManager.StrAnimationTrigger);

        GamePlayManager.GetInstance.RefSoundManager.PlaySoundOneShot(SoundManager.StrLaser);

        IsOneTimeUsed = true;

        StartCoroutine(AfterAnimatonEvent(laser));
    }

    IEnumerator AfterAnimatonEvent (Animator laser)
    {
        yield return new WaitForSeconds(LaserAnimationClip.averageDuration);

        laser.gameObject.SetActive(false);
        laser.transform.parent = GamePlayManager.GetInstance.RefItemManager.transform;
    }
}   