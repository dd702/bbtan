﻿using UnityEngine;
using System.Collections;

public class ParticleAnimation : MonoBehaviour {

    ParticleSystem particleAni;
    void Awake()
    {
        particleAni = GetComponent<ParticleSystem>();
    }

    void Update()
    {
        particleAni.startColor = new Color(Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f), 1.0f);
    }
}
