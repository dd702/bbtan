﻿using UnityEngine;
using System.Collections;

public class ItemChangeDirection : ItemOneTurnDisposable, ItemEffect
{
    public override void ActivateItemEffect(Collider2D collision)
    {
        GamePlayManager.GetInstance.RefSoundManager.PlaySoundOneShot(SoundManager.StrChangeDirection);

        Bullet ball = collision.GetComponent<Bullet>();
        ball.Move(new Vector3(Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f), 0.0f).normalized, GamePlayManager.BallSpd);

        IsOneTimeUsed = true;
    }
}
