﻿using UnityEngine;
using System.Collections;

public enum CategoryBrick
{
    Normal,
    Double
}

public class Brick : MonoBehaviour
{
    private const string StrBall = "Ball";

    private CategoryBrick EnumCategoryBrick = CategoryBrick.Normal;
    private TweenColor TweenColorSprite = null;
    public TweenColor TweenColorLabel = null;
    private TweenScale TweenScaleBrick = null;
    private UISprite SpriteForColor = null;
    private UILabel LabelHP = null;
    public int HP = -1;

    protected void Awake()
    {
        init();
    }

    protected void init()
    {
        TweenColorSprite = transform.Find("Sprite Brick").GetComponent<TweenColor>();
        TweenColorLabel = transform.Find("Label HP").GetComponent<TweenColor>();
        TweenScaleBrick = GetComponent<TweenScale>();
        SpriteForColor = TweenColorSprite.GetComponent<UISprite>();
        LabelHP = TweenColorLabel.GetComponent<UILabel>();
    }

    protected void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == StrBall)
            SetHP(HP - 1, true);
    }

    protected void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == ItemManager.StrLaser)
            SetHP(HP - 1, true);
    }

    public void SetHP(int hp, bool isFromCollision)
    {
        //Data
        HP = hp;
        int currentTurn = GamePlayManager.GetInstance.PropCurrentTurn;
        Color colorOrigin = SpriteForColor.color;

        //UI
        LabelHP.text = HP.ToString();

        if (EnumCategoryBrick == CategoryBrick.Normal)
        {
            TweenColorSprite.ResetToBeginning();
            TweenColorSprite.from = colorOrigin;
            TweenColorSprite.to = new Color(1.0f, 1.0f - HP / (float)(currentTurn + 1), 100 / 255.0f);
            TweenColorSprite.PlayForward();

            TweenColorLabel.ResetToBeginning();
            TweenColorLabel.from = colorOrigin;
            TweenColorLabel.to = new Color(1.0f, 1.0f - HP / (float)(currentTurn + 1), 100 / 255.0f);
            TweenColorLabel.PlayForward();
        }
        else if (EnumCategoryBrick == CategoryBrick.Double)
        {
            SpriteForColor.color = new Color(100 / 255.0f, HP / (float)currentTurn, 1.0f);
            LabelHP.color = new Color(100 / 255.0f, HP / (float)currentTurn, 1.0f);
        }

        //Break
        if (HP == 0)
        {
            BreakBrick();
            return;
        }

        // MinusHP - Because of TweenScale, Only Use Func From CollisionEnter Event
        if (!isFromCollision)
            return;

        MinusBrickHP();
    }

    void BreakBrick()
    {
        GamePlayManager.GetInstance.RefBrickManager.ChangeToEmptyBrick(transform);
        GamePlayManager.GetInstance.MoveBrickBreakParticle(transform);

        GamePlayManager.GetInstance.RefSoundManager.PlaySoundOneShot(SoundManager.StrDictionaryPop);
    }

    void MinusBrickHP()
    {
        int currentTurn = GamePlayManager.GetInstance.PropCurrentTurn + 1;
        //int currentTurn = 6;

        TweenScaleBrick.ResetToBeginning();
        TweenScaleBrick.PlayForward();
        StartCoroutine(PlayBackTweenScale());

        GamePlayManager.GetInstance.RefSoundManager.PlaySoundOneShot(SoundManager.StrDictionaryPop);
    }

    IEnumerator PlayBackTweenScale()
    {
        yield return new WaitForSeconds(TweenScaleBrick.duration);
        TweenScaleBrick.PlayReverse();
    }
}
